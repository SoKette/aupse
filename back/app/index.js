var express = require('express');
var app = express();
const https = require('https');
const options = {
  hostname: 'api.spacexdata.com',
  port: 443,
  path: '/v3/launches/latest',
  method: 'GET',
  Content : 'application/json',
}

app.get('/get-fusees', function (req,res)  {
    console.log('Received request for fusees from', req.ip);
    const request = https.request(options, (response) => {
      console.log(`statusCode: ${response.statusCode}`)
      response.on('data', (data) => {
        res.json(data);
        request.destroy();
      });
    });
    request.on('error', (error) => {
      console.error(error);
    });

    request.end();
});


app.get('/get-launches', function (req, res) {
  console.log('Received request for fusees from', req.ip)
  
  res.json(beerList);
})

app.use(express.static('../../front'));

var beerList = require('./json/fusee.json');
console.log("Beers", beerList);

var server = app.listen(3000, function () {
  var host = server.address().address;
  var port = server.address().port;
  console.log('Listening at http://%s:%s', host, port);
});